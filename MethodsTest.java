public class MethodsTest {
	public static void main(String[] args) {
		int x = 5;
		
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		
		methodTwoInputNoReturn(5, 5.5);
		
		int y = methodNoInputReturnInt();
		System.out.println(y);
		
		double sumSqRt = sumSquareRoot(9,5);
		System.out.println(sumSqRt);
		
		String s1 = "java";
		String s2 = "programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		System.out.println(SecondClass.addOne(50));
		
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
		
	}
	
	public static void methodNoInputNoReturn() {
		int x = 20;
		System.out.println("I'm in a method that takes no input and returns nothing");
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int num) {
		System.out.println("Inside the method input no return");
		num -= 5;
		System.out.println(num);
	}
	
	public static void methodTwoInputNoReturn(int intNum, double doubleNum) {
		System.out.println(intNum);
		System.out.println(doubleNum);
	}
	
	public static int methodNoInputReturnInt() {
		return 5;
	}
	
	public static double sumSquareRoot(int num1, int num2) {
		int sumSqRt = num1+num2;
		 return Math.sqrt(sumSqRt);
	}
}