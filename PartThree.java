import java.util.Scanner;
public class PartThree {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Please enter two values:");
		double valueOne = reader.nextDouble();
		double valueTwo = reader.nextDouble();
		
		Calculator calc= new Calculator();
		System.out.println("This is the sum of your two numbers: " + Calculator.add(valueOne, valueTwo));
		System.out.println("This is the difference of your two numbers: " + calc.subtract(valueOne, valueTwo));
		System.out.println("This is the product of your two numbers: " + Calculator.multiply(valueOne, valueTwo));
		System.out.println("This is the quotient of your two numbers: " + calc.divide(valueOne, valueTwo));
	}
}